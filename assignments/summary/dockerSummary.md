## Docker
        
- Docker is a program for developers to develop and run apps with containers. 
- A Docker image is an image that contains code, runtime libraries and environment variables.
- A container contains a docker image.

## Commands in Docker:
1. Viewing all containers: **docker ps**
2. Starting any stopped container: **docker start "id"**
3. Stopping any started container: **docker stop "id"**
4. Creating containers from images: **docker run "id"**
5. Removing a docker: **docker rm**
6. Downloading a docker: **docker pull "path"**
7. Copying a code inside a docker :**docker cp "prog_name" "id":/** 