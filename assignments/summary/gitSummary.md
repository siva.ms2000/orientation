Git is a version control software that  makes collaboration with my teammates . It makes the project more easy and collaborative to work.

## Vocabularies in git:
        1. Repo: A collection of files and folders that we are using git to track.
        2. Commit: Saving a work which exists at that moment which will exist only on the local machine
        3. Push: Syncing commits to the repository
        4. Branch:Seperate instances of a master that is different from the codebase
        5. Merge:The process of syncing the branch into the master branch.
        6. Cloning:Making an exact copy of the repository
        7. Forking:Similar to cloning, but creates a new repo under your name.

## States of a file:
        1. Modified: Changed the file, but not committed
        2. Staged: Marked a modified file in its current version to go into next snapshot
        3. Committed: Safely stored in the local machine

## Commands in git:
1. Clone using **git clone "url of the repo"**
2. Creating a new branch: 
                **git checkout master**
                **git checkout -b <branch name>**
3. Committing changes:
               **git commit -sv**